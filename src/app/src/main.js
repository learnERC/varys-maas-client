import axios from 'axios'
import Vue from 'vue'
import VueAxios from 'vue-axios'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'

// VueMoment
Vue.use(require('vue-moment'))

// VueAxios
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
