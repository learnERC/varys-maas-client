import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import colors from 'vuetify/es5/util/colors'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'fa',
  // theme: {
  //   primary: colors.shades.black,
  //   secondary: colors.grey.darken1,
  //   accent: colors.grey.lighten1,
  //   error: colors.red.accent4,
  //   warning: colors.yellow.base,
  //   info: colors.blue.base,
  //   success: colors.green.base
  // }
})
