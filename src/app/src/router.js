import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ChmmShow from './views/ChmmShow.vue'
import MonitoringRequestsList from './views/MonitoringRequestsList.vue'
import MonitoringRequestsCreate from './views/MonitoringRequestsCreate.vue'
import MonitoringRequestsEdit from './views/MonitoringRequestsEdit.vue'
import NotFound from './views/NotFound.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({
    y: 0
  }),
  base: process.env.PUBLIC_PATH,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/chmm-tree',
      name: 'chmm-tree.show',
      component: ChmmShow
    },
    {
      path: '/monitoring-requests',
      name: 'monitoring-requests.list',
      component: MonitoringRequestsList
    },
    {
      path: '/monitoring-requests/create',
      name: 'monitoring-requests.create',
      component: MonitoringRequestsCreate
    },
    {
      path: '/monitoring-requests/:id/edit',
      name: 'monitoring-requests.edit',
      component: MonitoringRequestsEdit,
      props: true
    },
    {
      path: '*',
      name: 'not-found',
      component: NotFound
    },
  ]
})
