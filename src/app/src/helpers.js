export function capitalize(value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

export function replace(value, from, to) {
  if (!value) return ''
  value = value.toString()
  return value.replace(new RegExp(from, 'g'), to)
}

export function transformTree(nodes) {
  for (const node of nodes) {
    node.id = node.name
    node.name = capitalize(
      replace(node.name, '_', ' ')
    )
    if (node.children) {
      transformTree(node.children)
    }
  }
  return nodes
}

export function extractTreeParents(tree) {
  // Filter tree with only parents of selections
  tree = tree.filter(elem => {
    for (let i = 0; i < tree.length; i++) {
      // Skip current element
      if (tree[i].id === elem.id) continue;
      // Check only elements with childrens
      if (tree[i].children) {
        let item = findTreeItem([tree[i]], elem.id);
        // If current element is a children of another element, exclude from result
        if (item) {
          return false;
        }
      }
    }
    return true;
  });
  return tree;
}

export function findTreeItem(items, id) {
  if (!items) {
    return;
  }
  for (const item of items) {
    // Test current object
    if (item.id === id) {
      return item;
    }
    // Test children recursively
    const child = findTreeItem(item.children, id);
    if (child) {
      return child;
    }
  }
}
