import Api from './Api'

class Chmm {
  async get() {
    return Api.get('chmm')
  }
}

export default new Chmm()