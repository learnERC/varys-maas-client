import Api from './Api'

class MonitoringTargets {
  async all(params) {
    return Api.get('monitoring-targets', {
      params
    })
  }
}

export default new MonitoringTargets()
