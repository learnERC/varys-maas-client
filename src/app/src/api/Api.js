import axios from 'axios'

class Api {
  constructor() {
    this.apiVersion = 'v1'
    this.apiHost = process.env.VUE_APP_MAAS_SERVER_HOST || '$VUE_APP_MAAS_SERVER_HOST'
    this.apiPort = process.env.VUE_APP_MAAS_SERVER_PORT || '$VUE_APP_MAAS_SERVER_PORT'
    this.baseURL = `http://${this.apiHost}:${this.apiPort}/api/${this.apiVersion}`
  }

  get(endpoint, options = {}) {
    endpoint = this._cleanEndpoint(endpoint)

    return axios.get(`${this.baseURL}/${endpoint}`, options)
      .then((response) => {
        return Promise.resolve(response.data)
      })
      .catch(error => console.log(error))
  }

  post(endpoint, data = {}, options = {}) {
    endpoint = this._cleanEndpoint(endpoint)

    return axios.post(`${this.baseURL}/${endpoint}`, data, options)
      .then((response) => {
        return Promise.resolve(response.data)
      })
      .catch(error => console.log(error))
  }

  patch(endpoint, data = {}, options = {}) {
    endpoint = this._cleanEndpoint(endpoint)

    return axios.patch(`${this.baseURL}/${endpoint}`, data, options)
      .then((response) => {
        return Promise.resolve(response.data)
      })
      .catch(error => console.log(error))
  }

  delete(endpoint, options = {}) {
    endpoint = this._cleanEndpoint(endpoint)
    return axios.delete(`${this.baseURL}/${endpoint}`, options)
      .then((response) => {
        return Promise.resolve(response.data)
      })
      .catch(error => console.log(error))
  }

  _cleanEndpoint(endpoint) {
    if (endpoint.startsWith('/')) {
      endpoint = endpoint.substring('/')
    }
    return endpoint
  }
}

export default new Api()
