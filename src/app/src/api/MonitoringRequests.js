import Api from './Api'

class MonitoringRequests {
  async get(id) {
    return Api.get(`monitoring-requests/${id}`)
  }

  async all(params) {
    return Api.get('monitoring-requests', {
      params
    })
  }

  async update(id, monitoring) {
    return Api.patch(`monitoring-requests/${id}`, { ...monitoring
    })
  }

  async create(monitoring) {
    return Api.post(`monitoring-requests`, { ...monitoring
    })
  }

  async save() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, 750)
    })
  }

  async delete(id) {
    return Api.delete(`monitoring-requests/${id}`)
  }
}

export default new MonitoringRequests()
