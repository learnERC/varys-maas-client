# VARYS MaaS Client

## Synopsis
This is a Proof of Concept (POC) for the VARYS MaaS Client.
The main aim is to provide a GUI to manage dynamic monitoring requests to the MaaS Server.

The application is built with [Vuetify](https://vuetifyjs.com/), a Material Vue.js component framework.

## How to execute
In order to create a reproducible environment a Dockerfile is available within root directory (`maas-client/`).

Also, a `docker-compose.yml` file is available to speed-up development and lay the foundations for a more complex setup.

> N.B.: all the commands are intended to be executed from the project root directory (`maas-client/`)

### Plain Docker execution
Build the image running:
```
$ docker build -t ngpaas/maas-client .
```

Then, create and run a new container based on the created image:
```
$ docker run -it \
  --name maas-client \
  --env-file .env \
  -p "8080:8080" \
  ngpaas/maas-client:latest
```
> N.B.: the specified port to bind MUST be the same configured as SERVICE_PORT within .env file

### Docker Compose execution
Requirements to run with Docker Compose:

* Docker Engine 18.02.0+
* Docker Compose 1.20.0+

Run the following command to build the services:
```
$ docker-compose build
```

Then, run the following command to run the services:
```
$ docker-compose up
```

## License
This project is licensed under the AGPLv3. See the [LICENSE.md](LICENSE.md) file for details.
